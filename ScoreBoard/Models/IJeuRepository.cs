﻿namespace ScoreBoard.Models
{
    public interface IJeuRepository
    {
        public List<Jeu> ListeJeux { get; set; }
        public Jeu? GetJeu(int jeuxId);
        public List<Jeu> GetJeuOfJoueur(int jeuxId);
        public int? GetJeuIndex(int jeuxId);
        public void Ajouter(Jeu jeu);
        public void Modifier(Jeu jeu);
        public void Supprimer(int jeuxId);
        public void SupprimerJeu(int jeuxId);
    }
}
