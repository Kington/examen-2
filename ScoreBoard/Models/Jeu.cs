﻿using System.ComponentModel.DataAnnotations;

namespace ScoreBoard.Models
{
    public class Jeu
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Nom { get; set; }
        [Required]
        [RegularExpression(@"^(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[0-2])/((19|20)\d\d)$", ErrorMessage = "La date doit être antérieure à la date du jour")]
        public DateTime DateSortie { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public int JoueurId { get; set; }
        [Required]
        public Joueur Joueur { get; set; }
        [Required]
        [Range(0, 100, ErrorMessage = "Désolé, le score du joueur doit être entre 0 à 100")]
        public int ScoreJoueur { get; set; }
        [Required]
        [RegularExpression(@"^(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[0-2])/((19|20)\d\d)$", ErrorMessage = "La date doit être antérieure à la date du jour")]
        public DateTime DateEnregistrement { get; set; }
    }
}
