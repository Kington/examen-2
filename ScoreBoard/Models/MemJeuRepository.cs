﻿namespace ScoreBoard.Models
{
    public class MemJeuRepository : IJeuRepository
    {
        public List<Jeu> _listejeux;
        
        public List<Jeu> ListeJeux { get => _listejeux; set => _listejeux = value; }

        public MemJeuRepository(IJoueurRepository joueurRepository)
        {
            _listejeux = new List<Jeu>
            {
                new Jeu { Id = 0, Nom = "The Legend of Zelda: Breath of the Wild", DateSortie = new DateTime(2017, 3, 3), Description = "Jeu d'action-aventure en monde ouvert", Joueur = joueurRepository.GetJoueur(0), ScoreJoueur = 60, DateEnregistrement = DateTime.Now },
                new Jeu { Id = 1, Nom = "Super Mario Odyssey", DateSortie = new DateTime(2017, 10, 27), Description = "Jeu de plateforme en monde ouvert", JoueurId = 1, Joueur = joueurRepository.GetJoueur(0), ScoreJoueur = 50, DateEnregistrement = DateTime.Now },
                new Jeu { Id = 2, Nom = "Red Dead Redemption 2", DateSortie = new DateTime(2018, 10, 26), Description = "Jeu d'action-aventure en monde ouvert dans le Far West", Joueur = joueurRepository.GetJoueur(0), ScoreJoueur = 100, DateEnregistrement = DateTime.Now },
                new Jeu { Id = 3, Nom = "Assassin's Creed Odyssey", DateSortie = new DateTime(2018, 10, 5), Description = "Jeu d'action-aventure en monde ouvert dans la Grèce antique", Joueur = joueurRepository.GetJoueur(1), ScoreJoueur = 100, DateEnregistrement = DateTime.Now },
                new Jeu { Id = 4, Nom = "God of War", DateSortie = new DateTime(2018, 4, 20), Description = "Jeu d'action-aventure en monde ouvert inspiré de la mythologie nordique", Joueur = joueurRepository.GetJoueur(1), ScoreJoueur = 30, DateEnregistrement = DateTime.Now },
                new Jeu { Id = 5, Nom = "Cyberpunk 2077", DateSortie = new DateTime(2020, 12, 10), Description = "Jeu de rôle en monde ouvert futuriste", Joueur = joueurRepository.GetJoueur(2), ScoreJoueur = 70, DateEnregistrement = DateTime.Now},
                new Jeu { Id = 6, Nom = "The Last of Us Part II", DateSortie = new DateTime(2020, 6, 19), Description = "Jeu d'action-aventure et de survie post-apocalyptique", Joueur = joueurRepository.GetJoueur(3), ScoreJoueur = 100, DateEnregistrement = DateTime.Now },
                new Jeu { Id = 7, Nom = "Animal Crossing: New Horizons", DateSortie = new DateTime(2020, 3, 20), Description = "Jeu de simulation de vie en monde ouvert", Joueur = joueurRepository.GetJoueur(3), ScoreJoueur = 10, DateEnregistrement = DateTime.Now },
                new Jeu { Id = 8, Nom = "Doom Eternal", DateSortie = new DateTime(2020, 3, 20), Description = "Jeu de tir à la première personne", Joueur = joueurRepository.GetJoueur(3), ScoreJoueur = 90, DateEnregistrement = DateTime.Now },
                new Jeu { Id = 9, Nom = "Ghost of Tsushima", DateSortie = new DateTime(2020, 7, 17), Description = "Jeu d'action-aventure en monde ouvert dans le Japon féodal", Joueur = joueurRepository.GetJoueur(4), ScoreJoueur = 100, DateEnregistrement = DateTime.Now },
                new Jeu { Id = 10, Nom = "Hades", DateSortie = new DateTime(2020, 9, 17), Description = "Jeu de rôle d'action roguelike", Joueur = joueurRepository.GetJoueur(4), ScoreJoueur = 40, DateEnregistrement = DateTime.Now }

            };
        }

        public Jeu GetJeu(int jeuxId)
        {
            return _listejeux.Find(x => x.Id == jeuxId);
        }

        public int? GetJeuIndex(int jeuxId)
        {
            return _listejeux.FindIndex(x => x.Id ==jeuxId);
        }

        public List<Jeu> GetJeuOfJoueur(int jeuxId)
        {
            return _listejeux.FindAll(x => x.JoueurId ==jeuxId);
        }

        public void Ajouter(Jeu jeu)
        {
            _listejeux.Add(jeu);
        }

        public void Modifier(Jeu jeu)
        {
            int? indexJeux = GetJeuIndex((int)jeu.Id);

            if (indexJeux != null)
            {
                _listejeux[indexJeux.Value] = jeu;
            }
        }

        public void Supprimer(int jeuxId)
        {
            Jeu jeu = GetJeu(jeuxId);
            _listejeux.Remove(jeu);
        }

        public void SupprimerJeu (int jeuxId)
        {
            _listejeux.RemoveAll(x => x.JoueurId == jeuxId);
        }
    }
}
