﻿namespace ScoreBoard.Models
{
    public interface IJoueurRepository
    {
        public List<Joueur> ListeJoueurs { get; set; }
        public Joueur? GetJoueur(int id);
        public int? GetJoueurIndex(int id);
        public void Ajouter(Joueur joueur);
        public void Modifier(Joueur joueur);
        public void Supprimer(int id);
        public void AJourJeu(int jeuxId, Jeu jeu);
    }
}
