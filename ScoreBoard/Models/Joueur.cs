﻿using System.ComponentModel.DataAnnotations;

namespace ScoreBoard.Models
{
    public class Joueur
    {
        [Required]
        [Display(Name = "Le Id")]
        public int Id { get; set; }
        [Required]
        [StringLength(20, ErrorMessage = "Le nom doit avoir entre 2 et 20 caractères", MinimumLength = 2)]
        [Display(Name = "Le nom du joueur")]
        public string Nom { get; set; }
        [Required]
        [StringLength(20, ErrorMessage = "Le prénom doit avoir entre 2 et 20 caractères", MinimumLength = 2)]
        [Display(Name = "Le prénom du joueur")]
        public string Prenom { get; set; }
        [Display(Name = "L'équipe du joueur")]
        [RegularExpression(@"([A-Z]{2,4})", ErrorMessage ="Le nom d'équipe doit avoir entre 2 et 4 majuscules")]
        public string? Equipe { get; set; }
        [Display(Name = "Le numéro de téléphone du joueur")]
        public string? Telephone { get; set; }
        [Required]
        [Display(Name = "Le courriel du joueur")]
        [RegularExpression(@"(^[0-9]+(?!.*(?:\+{2,}|\-{2,}|\.{2,}))(?:[\.+\-]{0,1}[a-z0-9])*@scoreboard\.ca$)", ErrorMessage = "L'email doit respecter le format identifiant@scoreboard.ca")]
        public string Courriel { get; set; }
        [Required]
        [Display(Name = "Le jeu choisi par le joueur")]
        public List<Jeu>? Jeux { get; set; }
    }
}
