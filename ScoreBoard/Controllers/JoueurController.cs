﻿using Microsoft.AspNetCore.Mvc;
using ScoreBoard.Models;

namespace ScoreBoard.Controllers
{
    public class JoueurController : Controller
    {
        private readonly IJoueurRepository _joueurRepository;
        public JoueurController(IJoueurRepository joueurRepository)
        {
            this._joueurRepository = joueurRepository;  
        }

        public ViewResult Index()
        {
            return View(_joueurRepository.ListeJoueurs);
        }
        [HttpGet]
        public ActionResult Creer()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Creer(Joueur joueur)
        {
            if (ModelState.IsValid)
            {
                _joueurRepository.Ajouter(joueur);
                return RedirectToAction("Index", _joueurRepository);
            }
            else
            {
                return View(joueur);
            }
              
            
            
            
        }
        [HttpGet]
        public ViewResult Modifier(int id)
        {
            Joueur joueur = _joueurRepository.GetJoueur(id);
            return View(joueur);
        }
        [HttpPost]
        public ActionResult Modifier(Joueur joueur)
        {
            _joueurRepository.Modifier(joueur);
            return RedirectToAction("Index");
        }
        public ActionResult Supprimer(int id)
        {
            _joueurRepository.Supprimer(id);
            return View();
        }
    }
}
