﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using ScoreBoard.Models;

namespace ScoreBoard.Controllers
{
    public class JeuController : Controller
    {
        private IJeuRepository _jeuRepository;
        private IJoueurRepository _joueurRepository;
        private List<SelectListItem> _selectJoueur;

        public JeuController(IJeuRepository jeuRepository, IJoueurRepository joueurRepository)
        {
            _jeuRepository = jeuRepository;
            _joueurRepository = joueurRepository;
            _selectJoueur = _joueurRepository.ListeJoueurs.Select(s => new SelectListItem { Value = s.Id.ToString(), Text = s.Prenom + ' ' + s.Nom}).ToList();
        }

        public ViewResult Index(int? jeuxId)
        {
            if (jeuxId != null)
            {
                List<Jeu> jeuOfJoueur = _jeuRepository.GetJeuOfJoueur(jeuxId.Value);
                return View(jeuOfJoueur);
            }
            return View(_jeuRepository.ListeJeux);
        }
        [HttpGet]
        public ViewResult Creer(int? jeuxId)
        {
            ViewBag.Joueurs = _selectJoueur;
            ViewBag.jeuxId = jeuxId;
            return View();
        }
        [HttpPost]
        public ActionResult Creer(Jeu jeu)
        {
            jeu.Joueur = _joueurRepository.GetJoueur(jeu.JoueurId);
            _jeuRepository.Ajouter(jeu);

            _joueurRepository.AJourJeu(jeu.JoueurId, jeu);

            return RedirectToAction("Index");
        }
        [HttpGet]
        public ViewResult Modifier(int id)
        {
            Jeu jeu = _jeuRepository.GetJeu(id);
            ViewBag.Joueurs = _selectJoueur;
            return View(jeu);
        }
        [HttpPost]
        public ActionResult Modifier(Jeu jeu)
        {
            _jeuRepository.Modifier(jeu);
            return RedirectToAction("Index");
        }

        public ActionResult Supprimer(int id)
        {
            _jeuRepository.Supprimer(id);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ViewResult Details(int id)
        {
            Jeu jeu = _jeuRepository.GetJeu(id);
                return View(jeu);
        }
    }
}
